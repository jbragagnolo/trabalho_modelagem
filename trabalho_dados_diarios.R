##Ajuste para seu diretório de trabalho
start.time <- Sys.time()
#setwd('C:\Users\Gabriel\Desktop\TrabalhoModelagem\trabalho_modelagem')
#dados = read.table ("dados/dados2.txt",head=T,comment.char = '#', sep = ';',dec = ".") ### Muitos dados
dados = read.table ("dados/dados_teste.txt",head=T,comment.char = '#', sep = ';',dec = ".") ### lê peq qtd dados para teste
#head(dados)


for (i in 1:nrow(dados)){ # coloca as duas leituras diarias em uma unica linha, tudo na leitura das 00:00
  dados$Precipitacao[i] = ifelse(dados$Hora[i] == "00:00",dados$Precipitacao[i+1] ,dados$Precipitacao[i])
  dados$TempMinima[i] = ifelse(dados$Hora[i] == "00:00",dados$TempMinima[i+1] ,dados$TempMinima[i])
}

dados = subset(dados, dados$Hora == "00:00",select = c("Data","Hora","TempMaxima","TempMinima","Insolacao","Umidade.Relativa.Media")) # Selecionado somente leituras das 00:00
dados$dateD = as.Date(dados$Data,format = "%d/%m/%Y") # cria coluna em formato de data
dados$tmed <- (dados$TempMaxima+dados$TempMinima)/2 # calculo temperatura média
dados = na.omit(dados) # remove dados nulos
row.names(dados)=NULL # remove coluna row.name (desnecessária)


dados$HoraDia = 0    # cria colnuna para hora do dia
dados$Hora = NULL    # remove coluna Hora
horas = 23
j = 1
i=1
linhas_dados=nrow(dados) # numero de linhas

for (i in 1:linhas_dados ) {   # adicinado uma linha para cada hora do dia
  cat ("executando (",i,"/",linhas_dados,")\n")
  for (j in 1:horas)  {
    newrow = list (dados$Data[i],dados$TempMaxima[i],dados$TempMinima[i],dados$Insolacao[i],dados$Umidade.Relativa.Media[i],dados$dateD[i],dados$tmed[i],j,44) # nova linha a ser adicionada 
    dados[nrow(dados) + 1,] = newrow ## adiciona nova linha no final do frame
  }
}


dados$TempHora = 44  # cria coluna para temperatura horaria


###NECESSÁRIO INSTALAR PACOTE plyr PARA USAR arrange (para ordernar pela coluna especifica)
library(plyr)
dados = arrange(dados,dados$dateD) # ordena dados pela data

linhas_dados=nrow(dados)
for (i in 1:linhas_dados){        # Calculo da temperatura horaria 
  if (dados$HoraDia[i] <= 9 ){    
    dados$TempHora[i]=((dados$TempMaxima[i]-dados$TempMinima[i])/2) * tanh((dados$HoraDia[i]-4.5)/2.5) + ((dados$TempMaxima[i]+dados$TempMinima[i])/2)
  } else {
    
    dados$TempHora[i]=-(((dados$TempMaxima[i]-dados$TempMinima[i])/2)) * tanh ((dados$HoraDia[i] - 16.5) / 3.5) + ((dados$TempMaxima[i]+dados$TempMinima[i])/2)
  }
  
}

linhas_dados=nrow(dados)
linhas_dados
#i = 1
j = 0
aux = 0
horas = 23


aux=0
for (i in 1:linhas_dados){        # Ajuste da hora para menor temperatura ficar as 04h
  if (dados$HoraDia[i] < 18 ){
    dados$HoraDia[i] = dados$HoraDia[i]+6
  } else {
    if (dados$HoraDia[i] == 18){
      aux = 0
    }
    dados$HoraDia[i] = aux
    aux=aux+1
  }
}

dados$ordenacao = as.numeric (paste(format (dados$dateD,"%j%Y"),dados$HoraDia,sep = ""))  ### cria uma coluna para ordenar

dados = arrange(dados,dados$ordenacao)  # ordena

#umDia = subset(dados, dados$dateD == "1962-02-01",select = c("TempHora","HoraDia"))
#plot (umDia$HoraDia,umDia$TempHora,type = "l")


########   CALCULO PRESSÃO ATMOSFÉRICA
### referencia: http://keisan.casio.com/exec/system/1224579725

#Estação           : PASSO FUNDO - RS (OMM: 83914)
#Latitude  (graus) : -28.21
#Longitude (graus) : -52.4
#Altitude  (metros): 684.05
latitude=-28.21
longitude=-52.4
altitude=684.05

pressaoMediaNivelMar=1013.25  #hPa

dados$pressao_hPa=pressaoMediaNivelMar*(1-((0.0065*altitude)/(dados$TempHora+0.0065*altitude+273.5)))^5.257    # Calculo da pressão usando altitude x temperatura x pressão média ao nivel do mar

attach(dados)
dados=dados[order(dateD,HoraDia),]
detach(dados)

# umDia = subset(dados, dados$dateD == "1962-02-01",select = c("TempHora","pressao_hPa","HoraDia"))
# plot (umDia$HoraDia,umDia$pressao_hPa,type = "l")


## http://www.climaonline.com.br/art_pdf/calc_po.pdf
#####dados$ponto_orvalho = dados$TempHora - (14.55 + 0.114 * dados$TempHora) * (1 - (0.01 * dados$Umidade.Relativa.Media)) - ((2.5 + 0.007 * dados$TempHora) * (1 - (0.01 * dados$Umidade.Relativa.Media)))^3 - (15.9 + 0.117 * dados$TempHora) * (1 - (0.01 * dados$Umidade.Relativa.Media))^14

#Pressão de saturação de vapor (es)
dados$pressao_vapor_saturado=0.61078*10^((7.5 * dados$TempHora) / (dados$TempHora + 273.3))

#pressão de vapor atual ea 
dados$pressao_vapor=(dados$Umidade.Relativa.Media/100)*dados$pressao_vapor_saturado
##### artigo podre
dados$ponto_orvalho = dados$TempHora - (273.3*log(dados$pressao_vapor)/0.611)/(7.5-log(dados$pressao_vapor)/0.611)

##3# http://andrew.rsmas.miami.edu/bmcnoldy/Humidity.html
#dados$ponto_orvalho = 243.04*(log(dados$Umidade.Relativa.Media/100)+((17.625*dados$TempHora)/(243.04+dados$TempHora)))/(17.625-log(dados$Umidade.Relativa.Media/100)-((17.625*dados$TempHora)/(243.04+dados$TempHora))) 

# wiki
#dados$ponto_orvalho = dados$TempHora - ( (100 - dados$Umidade.Relativa.Media) / 5)


end.time <- Sys.time()
time.taken <- end.time - start.time
time.taken

# tail(dados)
# dados[2,]


write.table(dados,file = "dados/lero.txt",sep = ";",row.names = F)
