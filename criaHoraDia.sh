#!/bin/bash

ARQ_DADOS=$1

[ $# -ne 1 ] && echo "Informe o arquivo de dados por parametro!!!" && exit 1

ARQ_NOVO=${ARQ_DADOS}_horaDia

[ -f $ARQ_NOVO ] && rm -f $ARQ_NOVO

linha=1
while read line
do
	if [ $linha -eq 1 ]  ### ignora linha header
	then
		linha=$((linha+1))
		novaLinha="$line,horaDia"
		echo $novaLinha >> $ARQ_NOVO
		continue
	fi
	
	for i in `seq 0 23`
	do
		novaLinha="$line,$i"
		echo $novaLinha >> $ARQ_NOVO
	done
	
	linha=$((linha+1))
done < $ARQ_DADOS
